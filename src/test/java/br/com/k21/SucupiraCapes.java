package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class SucupiraCapes extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver.exe");
		return new ChromeDriver();
	}

	@Test
	public void simulacaoLoginSucupiraIncorreto() throws InterruptedException {
		goTo("https://sucupira.capes.gov.br");
		click("#cabecalho > div > div.col-sm-4.col-md-4.col-lg-3 > a > p");
		
		fill("#login").with("admin");
		fill("#senha").with("admin");
		click("#saveButton");
		
		await().atMost(20000).until("#msgerro").isPresent();
		
		Assert.assertEquals("Login ou senha Inválidos", $("#msgerro").getText());
				
	}
}
