package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class BuscaAcordao extends FluentTest {
	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver.exe");
		return new ChromeDriver();
	}
	
	@Test
	public void buscaPNAE() throws InterruptedException {
		String nomePesquisado = "Coutinho";
		goTo("https://pesquisajuris.tjdft.jus.br/IndexadorAcordaos-web/sistj?visaoId=tjdf.sistj.acordaoeletronico.buscaindexada.apresentacao.VisaoBuscaAcordao");
		fill(BuscaAntiga.idCampoDePesquisa).with(nomePesquisado);
		click("#id_comando_pesquisar");
		
		Assert.assertTrue($(".conteudoComRotulo").get(1).getText().contains("coutinho"));
		
	}
}
