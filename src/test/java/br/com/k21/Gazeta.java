package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class Gazeta extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		return new ChromeDriver();
	}

	@Test
	public void simulacaoLoginSucupiraIncorreto() throws InterruptedException {
		goTo("https://www.gazetaonline.com.br/recuperarsenha");
		// click("#cabecalho > div > div.col-sm-4.col-md-4.col-lg-3 > a > p");
		
		fill("#email-forgot").with("1234");
		//fill("#senha").with("admin");
		click("#forgot-box-gol");
		
		Thread.sleep(3000);
		
		Assert.assertEquals("#ERRL05 - E-mail inválido, tente um e-mail existente.", $("#error").getText());
				
	}
}
