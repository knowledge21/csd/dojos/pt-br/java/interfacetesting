package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class Toro extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		return new ChromeDriver();
	}
	//#sidebar > div:nth-child(6) > span > span > span > a > h3
	@Test
	public void pesquisaPETR4() throws InterruptedException {
		String searchLink = "#global-wrapper > stockhome > stockhome-comparative > div > div.stock-filter-bar > div > hub-search > div > div.search-container > div > div > div > div > div.col-xs-24.col-md-6.col-lg-4.search-content > input";		
		int TEMPO_MAXIMO_TELA_BOLSA = 20000;
		String nomeDoPapel = "PETR4";
		
		goTo("https://www.toroinvestimentos.com.br/");
		click("#global-wrapper > home-wrap > div > app-home > hub-nav > nav > div > div > div > div.hidden-xs.hidden-sm.navbar-col.col-md-10 > ul > li:nth-child(1) > a");

		Thread.sleep(2000);

		click("#overlay-choice > div:nth-child(3) > div > div.overlay-choice-column.col-xs-10 > div > h2");
		
		await().atMost(TEMPO_MAXIMO_TELA_BOLSA).until(searchLink).isPresent();

		fill(searchLink).with(nomeDoPapel);
		
		Assert.assertEquals("Login ou senha Inválidos", $("#msgerro").getText());
				
	}
}
