package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class Lego extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		return new ChromeDriver();
	}
	//#sidebar > div:nth-child(6) > span > span > span > a > h3
	@Test
	public void simulacaoLoginSucupiraIncorreto() throws InterruptedException {
		goTo("https://aimfmuadministratoruihom.azurewebsites.net/#/login");
		String inputLogin = "body > div.margin-top-negative-50.ng-scope > div > div > form > div:nth-child(1) > input";
		String inputSenha = "body > div.margin-top-negative-50.ng-scope > div > div > form > div:nth-child(2) > input";
		String buttonSave = "body > div.margin-top-negative-50.ng-scope > div > div > form > button";
		String modalMensagem = "#toast-container > div > div > div > div";
		
		await().atMost(20000).until(inputLogin).isPresent();
		fill(inputLogin).with("asfasfasd");
		fill(inputSenha).with("admin");
							
		click(buttonSave);
		
		await().atMost(20000).until(modalMensagem).isPresent();
		
		Assert.assertEquals("Login ou senha Inválidos", $(modalMensagem).getText());
				
	}
}
