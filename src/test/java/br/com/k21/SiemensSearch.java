package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class SiemensSearch extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		return new ChromeDriver();
	}
	
	@Test
	public void siemensSearch() throws InterruptedException {
		goTo("http://search.siemens.com/en/");
		fill("#search-query").with("teste");
		click("#search-form > fieldset > a > span > span");
		
	//	await().atMost(2000).until("#content-zone > div.left-content > div > div > div > div.search-results.gsa > div.search-header.clearfix > h2").isPresent();
		
		Assert.assertEquals("Results 1-10 for teste", $("#content-zone > div.left-content > div > div > div > div.search-results.gsa > div.search-header.clearfix > h2").getText());
				
	}
}
