package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import junit.framework.Assert;

public class Imdb extends FluentTest {
	
	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver.exe");
		return new ChromeDriver();
	}
	
	@Test
	public void buscaNoIMDb() throws InterruptedException {
		//ARRANGE
		goTo("https://www.imdb.com/");
		fill("#suggestion-search").with("O som ao redor");
		
		//ACT
		click("#suggestion-search-button > svg");
		click("#main > div > div.findSection > table > tbody > tr.findResult.odd > td.result_text > a");
		
		String nomeDiretorEsperado = "Kleber Mendonça Filho"; 
		
		String pathNomeDiretorReal = "#title-overview-widget > div.plot_summary_wrapper.localized > div.plot_summary > div:nth-child(2) > a";
		await().atMost(10000).until(pathNomeDiretorReal).isPresent();
		
		//ASSERT
		Assert.assertEquals(nomeDiretorEsperado, $(pathNomeDiretorReal).getText());
				
	}
}
