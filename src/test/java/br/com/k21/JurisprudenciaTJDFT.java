package br.com.k21;

import org.fluentlenium.adapter.FluentTest;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import junit.framework.Assert;

public class JurisprudenciaTJDFT extends FluentTest {

	@Override
	public WebDriver getDefaultDriver() {
		System.setProperty("webdriver.chrome.driver", "/home/lula/node_modules/chromedriver/chromedriver/chromedriver");
		return new ChromeDriver();
	}
	
	@Test
	public void simulacaoJurisa() throws InterruptedException {
		goTo("https://pesquisajuris.tjdft.jus.br");
		fill("#numero").with("1113497");

		click("#id_comando_pesquisar");
		click("#tabela_resultado > tbody > tr:nth-child(1) > td:nth-child(2) > span");
		Assert.assertEquals("Processo: 07062718520188070000", $("#tabelaResultado > tbody > tr > td.td_sercos_left > b:nth-child(5)").getText() );
				
	}
}
